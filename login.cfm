<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="charset" value="utf-8">
	<title>Coherent Charts Login</title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
	<style type="text/css">
		body, html {
		margin: 0;
		padding: 0;
		background-color: #2b69aa;
		font-family: 'PT Sans';
		}
		.login-box {
		margin: auto;
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		width: 500px;
		height: 340px;
		padding: 10px 40px 10px 40px;
		background-color: white;
		-webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
		}
		.form-message {
		margin: 0;
		padding-left: 10px;
		color: #2b69aa;
		font-size: 10pt;
		font-weight: bold;
		}
	</style>
</head>
<body>

	<cfif isDefined("form.submit")>
		<cfset sess = createObject("lib.cfmumps.apps.vista.session").login(form.accessCode, form.verifyCode)>

		<cfif session.vistaSession.success>
			<cfset session.loggedIn = true>
			<cflocation url="default.cfm" addtoken="no">
		<cfelse>
			<cfset formMessage = session.vistaSession.errorMessage>
			<cfset structClear(session)>
		</cfif>
	<cfelse>
		<cfif isDefined("url.formMessage")>
			<cfset formMessage = url.formMessage>
		<cfelse>
			<cfset formMessage = "">
		</cfif>
	</cfif>

	<div class="login-box">
		<h1>Charts Login</h1>
		<div class="form-message" id="form-message"><cfoutput>#formMessage#</cfoutput></div>
		<hr>
		<form name="login" method="post" action="login.cfm" role="form">
			<div class="form-group">
				<label for="accessCode">Access Code</label>
				<input type="password" 
				       id="accessCode" 
				       name="accessCode" 
				       class="form-control"
				       placeholder="Enter your VistA access code">
			</div>
			<div class="form-group">
				<label for="verifyCode">Verify Code</label>
				<input type="password" 
				       id="verifyCode" 
				       name="verifyCode" 
				       class="form-control"
				       placeholder="Enter your VistA verify code">
			</div>
			<button type="submit" class="btn btn-primary pull-right" name="submit">Submit</button>
		</form>
	</div>
	
</body>
</html>
