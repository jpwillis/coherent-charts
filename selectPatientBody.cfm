<cfset patientFile = createObject("lib.cfmumps.apps.vista.fileMan.file").open(2)>
<cfset patientRecords = patientFile.getRecords(patientFile.getAllEntryNumbers(), [".01", ".03", ".1", ".101"])>
<cfset patientQuery = patientFile.getQuery(patientRecords)>

<table id="patientSelectTable">
	<thead>
		<tr>
			<th>Patient Name</th>
			<th>DOB</th>
			<th>Room-Bed</th>
			<th>Ward Location</th>
		</tr>
	</thead>
	<tbody>
		<cfoutput query="#patientQuery#">
			<cfset dfn = patientQuery.__fmInternalEntryNumber>
			<tr onclick="chart.session.selectPatient('#dfn#');" style="cursor: pointer;">
				<td>#patientQuery["NAME"]#</td>
				<td>#dateFormat(patientQuery["DATE OF BIRTH"], "d mmm yyyy")#</td>
				<td>#patientQuery["ROOM-BED"]#</td>
				<td>#patientQuery["WARD LOCATION"]#</td>
			</tr>
		</cfoutput>
	</tbody>
</table>

