<!---
Coherent Logic Charts

Copyright (C) 2014 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
--->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="description" content="cfmumps phonebook demo">
	<meta name="author" content="Coherent Logic Development LLC">

	<title>Coherent Logic Charts</title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.1/css/jquery.dataTables.css">
	<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.css">
	<link rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>

<body>
	<cfif session.loggedIn NEQ true>
		<cflocation url="login.cfm" addtoken="no">
	</cfif>



            <div class="header cld-row" id="header-panel">
		    <div class="session-header">
			    <ul>
				    <li><cfoutput>#session.vistaSession.attributes.VISTALOGIN.DISPLAYNAME#</cfoutput></li>
				    <li><a href="logout.cfm">Log Out</a></li>
			    </ul>
		    </div>
		    <div class="header-caption" id="global-toolbar">
			    <cfinclude template="global-toolbar.cfm">
		    </div>
            </div>
            <div class="body cld-row" id="main-panel">
		    <div class="left col scroll-y" id="sidebar">
		    </div>
		    <div class="right col scroll-y" id="content">
		    </div>
            </div>
            <div class="footer cld-row" id="footer-panel">
		    <ul class="cld-tabs">
			    <cfloop array="#application.modulesArray#" index="module">

				    <cfset mod = application.modules[module]>
				    <cfif mod.settings.enabled EQ 1>
					    <cfif module EQ application.settings.defaultModule>
						    <cfset liClass = "cctab cld-tabs-selected">
					    <cfelse>
						    <cfset liClass = "cctab cld-tabs-unselected">
					    </cfif>
					    <cfoutput><li class="#liClass#" id="#module#">#application.modules[module][""]#</li></cfoutput>
				    </cfif>

			    </cfloop>
		    </ul>
            </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/1.10.1/js/jquery.dataTables.js"></script>
    <script src="//cdn.datatables.net/plug-ins/725b2a2115b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="js/app.js"></script>

    <cfloop array="#application.modulesArray#" index="module">
	    <cfset mod = application.modules[module]>
	    <cfif mod.settings.enabled EQ 1>
		    <cfoutput><script src="modules/#module#/module.js"></script></cfoutput>
	    </cfif>
    </cfloop>

    <script language="javascript">
	    $(document).ready(function () {
	            chart.onReady();	   
	    });
    </script>

    <div class="modal fade" id="dialog-box">
	    <div class="modal-dialog">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" 
					    class="close">
					    <span aria-hidden="true">&times;</span>
					    <span class="sr-only">Close</span>
				    </button>
				    <div id="dialog-header"></div>
			    </div>
			    <div class="modal-body" id="dialog-body">
			    </div>
			    <div class="modal-footer" id="dialog-footer">
				    <button type="button" class="btn btn-default">Close</button>
				    <button type="submit" 
					    id="dialog-ok"
					    class="btn btn-primary">OK</button>
			    </div>
		    </div>
	    </div>
    </div>

  </body>
</html>
                                 
