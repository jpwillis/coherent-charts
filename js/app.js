var modules = {

};

var chart = {

    session: {

	patient: {},

	requestPatient: function () {
	    var patientDialog = new chart.dialog({
		bodyURL: 'selectPatientBody.cfm',
		headerURL: 'selectPatientHeader.cfm',
		onBodyLoad: function () {
		    $("#patientSelectTable").DataTable();
		}
	    });

	    patientDialog.show();
	},

	selectPatient: function(dfn) {
	    $.get('Application.cfc?method=setPatient&dfn=' + dfn, function (data) {
		chart.session.patient = data;
		$("#dialog-box").modal('hide');
		$("#global-toolbar").load('global-toolbar.cfm', function (data) {
		    chart.loadModule(chart.app.activeModule, function () {
			modules[chart.app.activeModule].onPatientSelect(dfn);
		    });
		});
	    });
	}
	
    },

    app: {},

    onReady: function() {
	$.get('Application.cfc?method=getApplicationScope', function (data) {
	    app = data;

	    $.get('Application.cfc?method=getSessionScope', function (data) {
		chart.session.context = data;

		if(chart.session.context.dfn) {
		    chart.loadModule(app.settings.defaultModule);
		}
		else {
		    chart.app.activeModule = app.settings.defaultModule;
		    chart.session.requestPatient();
		}
	    });
	});

	$(".cctab").click(function(e) {
	    var module = e.target.id;

	    $("#" + chart.app.activeModule).removeClass('cld-tabs-selected');
	    $("#" + chart.app.activeModule).addClass('cld-tabs-unselected');
	    modules[chart.app.activeModule].onDeactivate();

	    chart.loadModule(module);
	});
    },

    enterFullscreen: function () {
	$("#content").removeClass("right");
	$("#content").removeClass("col");
	$("#content").removeClass("scroll-y");
	$("#main-panel").removeClass("body");
	$("#main-panel").removeClass("cld-row");
	$("#header-panel").removeClass("header");
	$("#header-panel").removeClass("cld-row");
	$("#footer-panel").removeClass("footer");
	$("#footer-panel").removeClass("cld-row");
	$("#header-panel").hide();
	$("#sidebar").hide();
	$("#footer-panel").hide();
	$("#content").addClass("fullscreen");
	$(document).keyup(function(e) {
	    if(e.keyCode == 27) {
		chart.exitFullscreen();
	    }
	});
	var content = document.getElementById("content");
	var rfs = content.requestFullScreen || content.webkitRequestFullScreen || content.mozRequestFullScreen;
	rfs.call(content);
/*	toastr.options = {
	    "closeButton": false,
	    "debug": false,
	    "positionClass": "toast-bottom-left",
	    "onclick": null,
	    "showDuration": "200",
	    "hideDuration": "1000",
	    "timeOut": "10000",
	    "extendedTimeOut": "1000",
	    "showEasing": "swing",
	    "hideEasing": "linear",
	    "showMethod": "fadeIn",
	    "hideMethod": "fadeOut"
	}
	toastr.info('Press ESC to exit fullscreen mode', modules[chart.app.activeModule].name);*/
    },

    exitFullscreen: function () {
	$("#content").addClass("right");
	$("#content").addClass("col");
	$("#content").addClass("scroll-y");
	$("#main-panel").addClass("body");
	$("#main-panel").addClass("cld-row");
	$("#header-panel").addClass("header");
	$("#header-panel").addClass("cld-row");
	$("#footer-panel").addClass("footer");
	$("#footer-panel").addClass("cld-row");
	$("#header-panel").show();
	$("#sidebar").show();
	$("#footer-panel").show();
	$("#content").removeClass("fullscreen");
	$(document).keyup(function(e) {
	    // do nothing
	});	
    },

    load: function(selector, url, done) {
	$(selector).load(url, function (data) {
	    if(data.indexOf("+++ 000 SESSION TIMEOUT") > -1) {
		location.replace("login.cfm");
	    }
	    else {
		if(done) {
		    done(data);
		}
	    }
	});
    },

    loadModule: function(module, onLoaded) {
	var contentURL = 'modules/' + module + '/' + app.modules[module].settings.startup.contentPanel;
	var sidebarURL = 'modules/' + module + '/' + app.modules[module].settings.startup.sidebarPanel;

	modules[module].baseURL = 'modules/' + module;

	$("#content").html('');
	$("#sidebar").html('');

	chart.app.activeModule = module;
	$("#" + chart.app.activeModule).addClass('cld-tabs-selected');

	chart.load("#content", contentURL, function (contentData) {
	    chart.load("#sidebar", sidebarURL, function (contentData) {
		modules[module].onActivate();
		if(onLoaded) {
		    onLoaded();
		}
	    });
	});
    },

    dialog: function(options) {
	var acc = this;

	chart.load("#dialog-body", options.bodyURL, function () {
	    if(options.onBodyLoad) {
		options.onBodyLoad();
	    }
	    chart.load("#dialog-header", options.headerURL, function () {
		if(options.form) {
		    $("#dialog-ok").attr("form", options.form);
		}
		if(options.formAction) {
		    $("#dialog-ok").attr("formaction", options.formAction);
		}
		if(options.formMethod) {
		    $("#dialog-ok").attr("formMethod", options.formMethod);
		}
		if(options.onLoad) {
		    options.onLoad(acc);
		}
		if(options.onHeaderLoad) {
		    options.onHeaderLoad();
		}
	    });
	});
	return(this);
    }
    
};

chart.dialog.prototype.show = function () {
    $("#dialog-box").modal('show');
};

chart.dialog.prototype.hide = function () {
    $("#dialog-box").modal('hide');
};