<cfcomponent displayName="Application" output="true" access="remote">

	<cfset this.Name = "CoherentCharts">
	<cfset this.ApplicationTimeout = CreateTimeSpan(0, 10, 0, 0)>
	<cfset this.sessionTimeOut = CreateTimeSpan(0, 0, 30, 0)>
	<cfset this.SessionManagement = true>
	<cfset this.SetClientCookies = true>

	<cfsetting requesttimeout="500" showdebugoutput="false" enablecfoutputonly="false">

	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="true">
		<cfparam name="application.modules" default="">
		<cfparam name="application.settings" default="">
		<cfparam name="application.modulesArray" default="">

		<cfset application.modules = createObject("lib.cfmumps.global").open("KBBMCHRT", ["modules"]).getObject()>
		<cfset application.settings = createObject("lib.cfmumps.global").open("KBBMCHRT", ["settings"]).getObject()>

		<cfset application.modulesArray = listToArray(structKeyList(application.modules))>

		<cfreturn true>
	</cffunction>

	<cffunction name="OnSessionStart" access="public" returntype="boolean" output="true">
		<cfparam name="session.loggedIn" type="boolean" default="false">
		<cfparam name="session.DUZ" default="">
		<cfparam name="session.userDisplayName" default="">
		<cfparam name="session.dfn" default="">


		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="true">
		<cfreturn true>
	</cffunction>

	<cffunction name="OnRequest" access="public" returntype="void" output="true">
		<cfargument name="TargetPage" type="string" required="true">

		<cfif NOT find("login.cfm", arguments.TargetPage)>
			<cfif session.loggedIn EQ false>
				<cfif find("default.cfm", arguments.TargetPage)>
					<cflocation url="login.cfm?formMessage=Session%20timed%20out" addtoken="no">
				<cfelse>
					<span style="display:none;">+++ 000 SESSION TIMEOUT</span>
				</cfif>
			<cfelse>
				<cfinclude template="#arguments.targetPage#">
			</cfif>
		<cfelse>
			<cfinclude template="#arguments.TargetPage#">	
		</cfif>

		<cfreturn>
	</cffunction>

	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true">

		<cfreturn>
	</cffunction>

	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false">
		<cfargument name="SessionScope" type="struct" required="true">
		<cfargument name="ApplicationScope" type="struct" required="true">

		<cfset SessionScope.loggedIn = false>

		<cfreturn>
	</cffunction>

	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false">
		<cfargument name="ApplicationScope" type="struct" required="false" default="#structNew()#">

		<cfreturn>
	</cffunction>

	<cffunction name="getApplicationScope" access="remote" returntype="void" output="true">
		<cfif session.loggedIn EQ true>
			<cfcontent type="application/json">
			<cfoutput>#serializeJSON(application)#</cfoutput>
		</cfif>
	</cffunction>

	<cffunction name="getSessionScope" access="remote" returntype="void" output="true">
		<cfif session.loggedIn EQ true>
			<cfcontent type="application/json">
			<cfoutput>#serializeJSON(session)#</cfoutput>
		</cfif>
	</cffunction>

	<cffunction name="setPatient" access="remote" returntype="void" output="true">
		<cfargument name="dfn" type="numeric" required="true">

		<cfif session.loggedIn EQ true>
			<cfset var patientFile = createObject("lib.cfmumps.apps.vista.fileMan.file").open(2)/>
			<cfset var patientRecord[1] = patientFile.getRecord(arguments.dfn, [".01", ".1", ".101"])/>
			<cfset var patientQuery = patientFile.getQuery(patientRecord)/>
			<cfset var util = createObject("lib.cfmumps.util")/>
			<cfset var name = lcase(patientQuery["NAME"])/>
			<cfset var firstName = util.getPiece(name, ",", 2)/>
			<cfset var lastName = util.getPiece(name, ",", 1)/>
			<cfset var name = util.titleCase(firstName & " " & lastName)/>
			
			<cfset session.dfn = url.dfn>
			<cfset session.patient.fullName = name>
			<cfset session.patient.record = patientRecord>
			<cfset session.patient.query = patientQuery>
			<cfset session.patient.firstName = firstName>
			<cfset session.patient.lastName = lastName>
			<cfset session.patient.notificationCount = 0>
			
			<cfcontent type="application/json">
			<cfoutput>#serializeJSON(session.patient)#</cfoutput>
		</cfif>
	</cffunction>

<!---
	<cffunction name="OnError" access="public" returntype="void" output="true">
		<cfargument name="Exception" type="any" required="true">
		<cfargument name="EventName" type="string" required="false" default="">

		<h1>Error</h1>
		<cfdump var="#Exception#">
		<cfreturn>
	</cffunction>
--->
</cfcomponent>
