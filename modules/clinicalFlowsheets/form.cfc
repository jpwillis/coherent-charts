<cfcomponent output="true">
	<cffunction name="render" returntype="void" access="public" output="true">
		<cfargument name="flowSheet" type="component" required="true">
		<cfargument name="dfn" type="numeric" required="true">

		<cfset categories = listToArray(structKeyList(arguments.flowSheet.categories))>

		<cfoutput>
		<form id="newFS" name="newFS">
		<input type="hidden" name="flowsheetName" id="flowsheetName" value="#flowSheet.name#">
		<input type="hidden" name="flowsheetID" id="flowsheetID" value="#flowSheet.id#">
		<input type="hidden" name="dfn" id="dfn" value="#arguments.dfn#">
		<cfset categoryIndex = 0>
		<cfloop array="#categories#" index="category">
			<cfif categoryIndex EQ 0>
				<cfset catStyle = "display:block;">
			<cfelse>
				<cfset catStyle = "display:none;">
			</cfif>
			<cfset categoryIndex += 1>
			<div id="category-#category#" style="#catStyle#" class="category-box">
			<legend>#category#</legend>
			<cfset categoryID = flowSheet.categories[category][""]>
			<cfset observations = listToArray(structKeyList(flowSheet.categories[category]))>
			<cfloop array="#observations#" index="observation">
				<div class="form-group">
				<legend>#observation#</legend>
				<cfset observationID = flowsheet.categories[category][observation][""]>
				<cfset controls = listToArray(structKeyList(flowSheet.categories[category][observation]["Controls"]))>


				<cfloop array="#controls#" index="control">
					<cfset controlStruct = flowSheet.categories[category][observation]["Controls"][control]>
					<cfset cID = controlStruct[""]>
					<cfset controlID = flowsheet.id & "." & categoryID & "." & observationID & "." & cID>
					<cfset this.renderControl(controlStruct, controlID)>
				</cfloop>
				
				<cfset pID = flowsheet.id & "." & categoryID & "." & observationID>

				<cfif flowsheet.categories[category][observation]["Units"]["enabled"] GT 0>
					<cfset units = "">
				<cfelse>
					<cfset units = 'disabled="disabled"'>
				</cfif>

				<cfif flowsheet.categories[category][observation]["Method"]["enabled"] GT 0>
					<cfset method = "">
				<cfelse>
					<cfset method = 'disabled="disabled"'>
				</cfif>

				<cfif flowsheet.categories[category][observation]["Location"]["enabled"] GT 0>
					<cfset location = "">
				<cfelse>
					<cfset location = 'disabled="disabled"'>
				</cfif>

				<cfif flowsheet.categories[category][observation]["Quality"]["enabled"] GT 0>
					<cfset quality = "">
				<cfelse>
					<cfset quality = 'disabled="disabled"'>
				</cfif>

				<cfif flowsheet.categories[category][observation]["Product"]["enabled"] GT 0>
					<cfset product = "">
				<cfelse>
					<cfset product = 'disabled="disabled"'>
				</cfif>				
				

				<cfif flowsheet.categories[category][observation]["Comment"]["enabled"] GT 0>
					<label class="fs-label" for="#pID#.Comment">Comments</label>
					<textarea  rows="3" style="width:100%; margin-bottom:10px;" name="#pID#.Comment" label="#pID#.Comment"></textarea>
				</cfif>
				<label class="fs-label" for="#pID#.Units">Units</label>
				<select class="metadata-select" name="#pID#.Units" id="#pID#.Units" #units#>

				</select>
				<label class="fs-label" for="#pID#.Product">Product</label>
				<select class="metadata-select" name="#pID#.Product" id="#pID#.Product" style="margin-bottom:40px;" #product#>

				</select>

				<label class="fs-label" for="#pID#.Method">Method</label>
				<select class="metadata-select" name="#pID#.Method" id="#pID#.Method" #method#>

				</select>
				<label class="fs-label" for="#pID#.Location">Anatomic Location</label>
				<select class="metadata-select" name="#pID#.Location" id="#pID#.Location" #location#>

				</select>
				<label class="fs-label" for="#pID#.Quality">Quality</label>
				<select class="metadata-select" name="#pID#.Quality" id="#pID#.Quality" #quality#>

				</select>

				</div>
			</cfloop>
			</div>
		</cfloop>
		</form>
		</cfoutput>


	</cffunction>

	<cffunction name="renderControl" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">
			
		<cfswitch expression="#s.type#">
			<cfcase value="date">
				<cfset this.renderDate(s, controlID)>
			</cfcase>
			<cfcase value="text">
				<cfset this.renderText(s, controlID)>
			</cfcase>
			<cfcase value="number">
				<cfset this.renderNumber(s, controlID)>
			</cfcase>
			<cfcase value="radioGroup">
				<cfset this.renderRadioGroup(s, controlID)>
			</cfcase>
			<cfcase value="checkboxGroup">
				<cfset this.renderCheckboxGroup(s, controlID)>
			</cfcase>			
		</cfswitch>
	</cffunction>

	<cffunction name="renderDate" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">
		<cfoutput>
			<div class="row">
			<div class="col-md-2">
			<label for="#controlID#">#s.label#</label>
			</div>
			<div class="col-md-10">
			<input type="date"
			       name="#controlID#"
			       id="#controlID#"
			       style="margin-bottom:10px;">
			</div>
			</div>
		</cfoutput>
			
	</cffunction>

	<cffunction name="renderText" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">

		
	</cffunction>

	<cffunction name="renderNumber" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">
		<cfoutput>
			<div class="row">
			<div class="col-md-2">
				<label for="#controlID#">#s.label#</label>
			</div>
			<div class="col-md-10">
				<input type="number"
				       name="#controlID#"
				       id="#controlID#"
				       min="#s.min#"
				       max="#s.max#"
				       style="margin-bottom: 10px;">
			</div>
			</div>
		</cfoutput>
	</cffunction>

	<cffunction name="renderRadioGroup" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">
		
		<cfoutput>
			<cfset opts = listToArray(structKeyList(s.options))>
			<cfset cols = round(opts.len() / 4)>
			<cfif cols GT 0>
				<cfset gridWidth = 12 / cols>
			<cfelse>
				<cfset gridWidth = 12>
			</cfif>			
			<cfset idx = 0>
			<cfset startCol = 1>
			<cfset endCol = 0>
			<cfset ctlCtr = 1>
			<div class="row">
			<cfloop array="#opts#" index="opt">				
				<cfif startCol EQ 1>
					<div class="col-md-#gridWidth#">
					<cfset startCol = 0>
					<cfset endCol = 0>
				</cfif>
				<input type="radio" 				    
				       value="#s.options[opt]#" 
				       name="#controlID#" 
				       id="#controlID#.#idx#">
				<label class="fs-label" for="#controlID#.#idx#">#opt#</label><br>
				
				<cfif ctlCtr EQ 4>
					<cfset endCol = 1>
					<cfset ctlCtr = 0>					
				</cfif>

				<cfif endCol EQ 1 OR idx EQ opts.len() - 1>
					</div>
				        <cfset startCol = 1>
				</cfif>

				<cfset ctlCtr += 1>
				<cfset idx += 1>
			</cfloop>
			</div>
		</cfoutput>
		
	</cffunction>

	<cffunction name="renderCheckboxGroup" returntype="void" access="public" output="true">
		<cfargument name="s" type="struct" required="true">
		<cfargument name="controlID" type="string" required="true">

		
	</cffunction>

	
</cfcomponent>
