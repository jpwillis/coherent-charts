<cfset db = createObject("lib.cfmumps.mumps")>
<cfset db.open()>

	<cfset lastResult = false>
	<cfset nextSubscript = "">

	<cfset flowsheets = []>

	<cfloop condition="lastResult EQ false">
		<cfset order = db.order("KBBMFSEN", [session.dfn, nextSubscript])>
		<cfset lastResult = order.lastResult>
		<cfset nextSubscript = order.value>
		<cfset os = {}>

		<cfif nextSubscript NEQ "">
			<cfset os.value = nextSubscript>
			<cfset year = left(os.value, 4)>
			<cfset month = mid(os.value, 5, 2)>
			<cfset day = mid(os.value, 7, 2)>		
			<cfset os.date = createDate(year, month, day)>
			<cfset os.dateFormatted = dateFormat(os.date, "mmmm d")>
			<cfset flowsheets.append(os)>
		</cfif>
	</cfloop>

	<cfoutput>
		<cfloop from="#flowsheets.len()#" to="1" index="idx" step="-1">			
			<div class="picker-row">
			<a onclick="modules.clinicalFlowsheets.viewFS(#session.dfn#, '#flowsheets[idx].value#');" href="##">
			<span class="glyphicon glyphicon-list"></span> #flowsheets[idx].dateFormatted#
			</a>
			</div>
		</cfloop>
	</cfoutput>

	<div class="dropdown" style="margin:20px;">
		<button class="btn btn-primary dropdown-toggle"
			type="button"
			id="flowsheetsMenu"
			data-toggle="dropdown">
			New Flowsheet
			<span class="caret"></span>
		</button>
		<cfset lastResult = false>
		<cfset nextSubscript = "">
		<ul class="dropdown-menu" role="menu" aria-labelledby="flowsheetsMenu">
			<cfloop condition="lastResult EQ false">
				<cfset order = db.order("KBBMFS", [nextSubscript])>
				<cfset lastResult = order.lastResult>
				<cfset nextSubscript = order.value>
				<cfoutput>
					<cfif nextSubscript NEQ "">
						<li role="presentation" id="ffzo">
							<a role="menuitem" 
							   tabindex="-1" 
							   href="##" 
							   onclick="modules.clinicalFlowsheets.newFS(#session.dfn#, '#order.value#');">
								#order.value#
							</a>
						</li>
					</cfif>
				</cfoutput>
			</cfloop>
		</ul>
	</div>



<cfset db.close()>
