modules.clinicalFlowsheets = {

    name: 'Clinical Flowsheets',

    onActivate: function () {

    },

    onDeactivate: function () {

    },

    onPatientSelect: function (dfn) {

    },

    viewFS: function (dfn, date) {

	var url = 'viewFS.cfm?dfn=' + escape(dfn) + '&date=' + escape(date);

	$("#content").load(url, function () {

	});
    },

    newFS: function (dfn, flowSheet) {
	var fsURL = this.baseURL + '/newFlowsheet.cfm?dfn=' + escape(dfn) + '&flowSheet=' + escape(flowSheet);
	var dropdownURL = this.baseURL + '/dropdown.cfm?flowSheet=' + escape(flowSheet);

	newFsDialog = new chart.dialog({
	    bodyURL: fsURL,
	    headerURL: dropdownURL,
	    form: "fsForm",
	    formAction: "submitFS.cfm",
	    formMethod: "post",
	    onLoad: function (obj) {
		obj.show();
	    }
	});
    },

    showCategory: function (category) {
	$(".category-box").hide();
	$("div[id*='" + category + "']").show();
    }

};