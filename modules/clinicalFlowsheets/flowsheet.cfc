<cfcomponent output="false">
	<cfset this.name = "">
	<cfset this.categories = {}>

	<cffunction name="open" returntype="component" access="public" output="false">
		<cfargument name="flowsheetName" type="string" required="true">

		<cfset this.name = arguments.flowsheetName>
		<cfset this.global = createObject("lib.cfmumps.global").open("KBBMFS", [arguments.flowsheetName])>
		<cfset this.categories = this.global.getObject()>
		<cfset this.id = this.global.db.get("KBBMFS", [arguments.flowsheetName])>

		<cfreturn this>
	</cffunction>

	<cffunction name="getObservation" returntype="struct" access="public" output="false">
		<cfargument name="dfn" type="numeric" required="true">
		<cfargument name="date" type="string" required="true">
		<cfargument name="time" type="string" required="true">
		<cfargument name="flowsheet" type="string" required="true">
		<cfargument name="category" type="string" required="true">
		<cfargument name="observation" type="string" required="true">
		<cfargument name="format" type="string" required="true">


		<cfset var db = createObject("lib.cfmumps.mumps")>
		<cfset db.open()>

		<cfset var fs = db.get("KBBMFS", [flowsheet])>
		<cfset var cat = db.get("KBBMFS", [flowsheet, category])>
		<cfset var obs = db.get("KBBMFS", [flowsheet, category, observation])>
		<cfset var g = createObject("lib.cfmumps.global").open("KBBMFS", [flowsheet, category, observation, "RF"])>
		<cfset var reportFormats = g.getObject()>

		<cfset var reportTemplate = reportFormats[arguments.format]>
		
		<cfset var subs = [dfn, date, time, fs, cat, obs]>
		<cfset var obj = createObject("lib.cfmumps.global").open("KBBMFSEN", subs).getObject()>
		
		<cfset var os = {}>
		<cfset os.reportTemplate = reportTemplate>

		<cfset var cookedVal = "">
		<cfset var currentSubscript = "">
		<cfset var inExpr = false>

		<cfloop from="1" to="#reportTemplate.len()#" index="i">
			<cfset char = mid(reportTemplate, i, 1)>

			<cfif char NEQ "{">
				<cfif inExpr>
					<cfif char NEQ "}">
						<cfset currentSubscript &= char>
					<cfelse>
						<cfset subs = [dfn, date, time, fs, cat, obs, currentSubscript]>
						<cfset tmp = db.get("KBBMFSEN", subs)>
						<cfif tmp NEQ "">
							<cfset cookedVal &= tmp>
						<cfelse>
							<cfset cookedVal &= "N/A">
						</cfif>
						<cfset currentSubscript = "">
						<cfset inExpr = false>
					</cfif>
				<cfelse>
					<cfset cookedVal &= char>
				</cfif>
			<cfelse>
				<cfset inExpr = true>
			</cfif>
		</cfloop>
		<cfset os.value = cookedVal>
		<cfif isDefined("obj.Comment")>
			<cfset os.comment = obj.Comment>
		<cfelse>
			<cfset os.comment = "">
		</cfif>
		<cfreturn os>
	</cffunction>

</cfcomponent>
