<cfset gblEntry = createObject("lib.cfmumps.global")>
<cfset gblEntry.open("KBBMFSEN", [session.dfn])>
<cfset entries = gblEntry.getObject()>

<cfset gblDefs = createObject("lib.cfmumps.global")>
<cfset gblDefs.open("KBBMFS", [])>
<cfset definitions = gblDefs.getObject()>

<cfset dates = listToArray(structKeyList(entries))>
<cfset dates.sort("numeric", "desc")>

<cfset flowsheets = listToArray(structKeyList(definitions))>

<table class="cld-table">
	<thead>
		<tr>
			<th>Observation</th>
			<cfloop array="#dates#" index="date">
				<cfset year = left(date, 4)>
				<cfset month = mid(date, 5, 2)>
				<cfset day = mid(date, 7, 2)>
				<cfset fDate = dateFormat(createDate(year, month, day), "m/dd/yyyy")>
				<cfset times = listToArray(structKeyList(entries[date]))>
				<cfset times.sort("text", "desc")>
				<cfloop array="#times#" index="time">
					<cfset hour = mid(time, 2, 2)>
					<cfset min = mid(time, 4, 2)>
					<cfset sec = mid(time, 6, 2)>
					<cfset fTime = timeFormat(createTime(hour, min, sec), "h:mm tt")>
					<th><cfoutput><strong>#fDate#</strong><br>#fTime#</cfoutput></th>
					<cfset fTime = "">
				</cfloop>
			</cfloop>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#flowsheets#" index="flowsheet">
			<tr>
				<th scope="row"><strong><cfoutput>#flowsheet#</cfoutput></strong></th>
				<cfloop array="#dates#" index="date">
					<cfset times = listToArray(structKeyList(entries[date]))>
					<cfloop array="#times#" index="time">
					<td>&nbsp;</td>
					</cfloop>
				</cfloop>
			</tr>
			<cfset categories = listToArray(structKeyList(definitions[flowsheet]))>
			<cfloop array="#categories#" index="category">
				<tr>
					<th scope="row" class="indent-1"><strong><cfoutput>#category#</cfoutput></strong></th>
					<cfloop array="#dates#" index="date">
						<cfset times = listToArray(structKeyList(entries[date]))>
						<cfloop array="#times#" index="time">
							<td>&nbsp;</td>
						</cfloop>
					</cfloop>
				</tr>

				<cfset observations = listToArray(structKeyList(definitions[flowsheet][category]))>
				<cfloop array="#observations#" index="observation">
				<tr>
					<th scope="row" class="indent-2"><cfoutput>#observation#</cfoutput></th>
					<cfloop array="#dates#" index="date">
						<cfset times = listToArray(structKeyList(entries[date]))>
						<cfloop array="#times#" index="time">
							<td nowrap>
								<cfset fs = createObject("flowsheet")>
								<cfset obs = fs.getObservation(session.dfn, date, time, flowsheet, category, observation, "summary")>
								<cfoutput>
									#obs.value#
									<cfif obs.comment NEQ "">						
										<span class="glyphicon glyphicon-comment" style="float: right;" title="#obs.comment#"></span>
									</cfif>								
								</cfoutput>
							</td>
						</cfloop>
					</cfloop>
				</tr>
				</cfloop>
			</cfloop>

		</cfloop>
	</tbody>
</table>


