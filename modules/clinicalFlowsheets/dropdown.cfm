<cfset fs = url.flowSheet>
<cfset db = createObject("lib.cfmumps.mumps").open()>

<cfset lastResult = false>
<cfset nextSubscript = "">

<div class="btn-group">
	<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
		<cfoutput>#fs#</cfoutput> <span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu">
	<cfloop condition="lastResult EQ false">
		<cfset order = db.order("KBBMFS", [fs, nextSubscript])>
		<cfset nextSubscript = order.value>
		<cfset lastResult = order.lastResult>

		<cfif nextSubscript NEQ "">
			<cfoutput>
				<li><a onclick="modules.clinicalFlowsheets.showCategory('#nextSubscript#');" href="##">#nextSubscript#</a></li>
			</cfoutput>
		</cfif>
	</cfloop>
	</ul>
</div>

<cfset db.close()>
