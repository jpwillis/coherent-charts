<cfset fields = listToArray(form.fieldNames)>

<cfset fsName = form.flowsheetName>
<cfset fsID = form.flowsheetID>
<cfset dfn = form.dfn>

<cfoutput>
	flowsheet name: #fsName#<br>
	flowsheet id: #fsID#<br>
	patient dfn: #dfn#<br>
		
</cfoutput>

<cfset db = createObject("lib.cfmumps.mumps")>
<cfset db.open()>
<cfset dateStr = dateFormat(Now(), "yyyymmdd")>
<cfset timeStr = "T" & timeFormat(Now(), "HHmmss")>
<cfloop array="#fields#" index="field">

	<cfset findStr = fsID & ".">
	<cfif find(findStr, field)>
		<!--- this is a flowsheet entry element --->	
		<cfset tField = dfn & "." & dateStr & "." & timeStr & "." & field>		
		<cfset tVal = form[field]>
		<cfset subs = listToArray(tField, ".")>

		<cfset db.set("KBBMFSEN", subs, tVal)>
	</cfif>
</cfloop>

<cflocation url="default.cfm?dfn=#dfn#" addtoken="no">
