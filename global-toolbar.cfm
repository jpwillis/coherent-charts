<cfoutput>
<div class="btn-group">
	<cfif isDefined("session.patient")>
		<button type="button" 
			title="Patient summary"
			class="btn btn-default">
			#session.patient.fullName# 
			<span class="badge"><cfif session.patient.notificationCount GT 0>#session.patient.notificationCount#</cfif></span>
		</button>

		<button type="button" 
			class="btn btn-default dropdown-toggle" 
			onclick="chart.session.requestPatient();" 
			title="Click to select a different patient">
			<span class="caret"></span>
		</button>
	</cfif>
</div>
<button type="button" 
	class="btn btn-default" 
	title="Present Fullscreen"
	onclick="chart.enterFullscreen();"><span class="glyphicon glyphicon-fullscreen"></span></button>
</cfoutput>
